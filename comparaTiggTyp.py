#!/usr/bin/python
# -*- coding: iso-8859-15 -*-


def initFunc (fileEventos,bytEventos):
	while 1:
		bytEventos=fileEventos.tell()
		line=fileEventos.readline()
		#print(line)
		#print (str(bytEventos))
		#if para ver si la línea empieza con el evento 1
		if line[0]=='1':
			break		
	return bytEventos

#bucle for para sacar el número de evento y trigger type de Eventos, en la línea leída
def datosEventos (lineEventos,contevento,triggEventos):
	cnt=0
	for cntstr in range(0,len(lineEventos)):
		if (lineEventos[cntstr]!='\t') and (cnt==0):
			contevento+=lineEventos[cntstr]
		elif lineEventos[cntstr]=='\t':
			cnt+=1
		elif (cnt==4):
			triggEventos+=lineEventos[cntstr]
	return [contevento, triggEventos]	


#función que analiza los archivos. Primero se lee el de Eventos y después el tcpip
def trueFunc (fileEventos,filetcp,fileresult,bytEventos):
	fileEventos.seek(bytEventos)

	contevento=""
	conttcp=""
	triggEventos=""
	triggtcp=""
	eventoAnterior=""

	lineEventos=fileEventos.readline()
	linetcp=filetcp.readline()

	#se lee un línea en Eventos y después se itera en tcpip hasta coincidencia en número de evento
	while lineEventos:
		eventoAnterior=contevento
		contevento=""
	 	triggEventos=""

	 	returned=datosEventos(lineEventos,contevento,triggEventos)
	 	contevento=returned[0]
	 	triggEventos=returned[1]

	 	#bucle para saltar los eventos duplicados, triplicados... en Eventos
	 	while contevento==eventoAnterior:
	 		eventoAnterior=contevento
			contevento=""
	 		triggEventos=""
	 		lineEventos=fileEventos.readline()
		 	returned=datosEventos(lineEventos,contevento,triggEventos)
	 		contevento=returned[0]
	 		triggEventos=returned[1] 		

		cnt=0
		#bucle para leer la línea de tcpip. Pasa a la siguiente línea si el contador de evento no coincide con él de la línea de Eventos
	 	while linetcp:
	 		conttcp=""
	 		triggtcp=""
	 		if len(linetcp)<3:
	 			pass
	 			#linetcp=filetcp.readline()
	 			#print("entro en el len()")
	 		elif (linetcp[0]!='\t') or (linetcp[1]!='\t') or (linetcp[2]=='0'):
	 			pass
	 			#linetcp=filetcp.readline()
	 			#print("entro en condición de ==0")
	 		else:	
				for cntstr in range(0,len(linetcp)):
					if linetcp[cntstr]=='\t':
						cnt+=1
					elif (linetcp[cntstr]!='\t') and (cnt==2):
						conttcp+=linetcp[cntstr]
					elif (cnt==11) and (linetcp[cntstr]!='\n'):
						triggtcp+=linetcp[cntstr]
				#print("Entro en el else "+conttcp+" "+triggtcp) 
			cnt=0	
			if (contevento)==(conttcp):
				linetcp=filetcp.readline()
				break
			else:
				linetcp=filetcp.readline()
				#print("Entro en el else")
	 	if (triggEventos)==(triggtcp):
	 		lineEventos=fileEventos.readline()
	 	elif (triggEventos)!=(triggtcp):
	 		string="Evento: "+contevento+" Trigger tcpip.txt: "+triggtcp+" Trigger Eventos_.txt: "+triggEventos+"\n"
	 		fileresult.write(string)
	 		lineEventos=fileEventos.readline()

	fileresult.write("Número de último evento tcpip:"+conttcp+" Número de último evento Eventos:"+contevento) 		

def main():
	fileEventos=open('Eventos22_12_20_17 20 50.txt','r') #Eventos22_12_20_17 20 50.txt
	filetcp=open('tcpip.txt','r') #tcpip.txt
	fileresult=open("triggerMismatch.txt","w") #Primero hay que borrar el contenido del archivo, si lo hay.
	fileresult.write("")
	fileresult.close()
	fileresult=open("triggerMismatch.txt","a")

	bytEventos=0

	bytEventos=initFunc(fileEventos,bytEventos)
	#pruebas
	# print (str(bytEventos))
	# print (fileEventos.readline())
	# fileEventos.seek(bytEventos)
	# print (len(fileEventos.readline()))
	trueFunc(fileEventos,filetcp,fileresult,bytEventos)

	fileresult.close()
	fileEventos.close()
	filetcp.close()

if __name__ == "__main__":

	main()